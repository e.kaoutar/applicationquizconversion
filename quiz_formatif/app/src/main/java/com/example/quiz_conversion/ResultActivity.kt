package com.example.quiz_conversion

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)


        intent?.let{
            val conv = intent.extras.getParcelable(MainActivity.CONVERTER) as laConversion


        var leModeInitial = conv.etatinitial.toString()
        var leModeFinal = conv.etatfinal.toString()

        when(leModeInitial){
            "Binaire" -> when(leModeFinal){
                            "Decimal" -> leResultat.text="Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertBinaryToDecimal(conv.nombreEntre.toString().toLong()).toString()
                            "Octal" -> leResultat.text = "Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertBinaryToOctal((conv.nombreEntre.toString().toLong())).toString()
                            "Hexa" -> leResultat.text = "Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertBinaryToHexa(conv.nombreEntre.toString().toLong()).toString()
                                }
            "Decimal" -> when(leModeFinal){
                            "Binaire" -> leResultat.text = "Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertDecimalToBinary(conv.nombreEntre.toString().toInt()).toString()
                            "Octal" -> leResultat.text = "Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertDecimalToOctal(conv.nombreEntre.toString().toInt()).toString()
                            "Hexa" -> leResultat.text = "Le résultat de conversion de ${conv.etatinitial} à ${conv.etatfinal} du nombre ${conv.nombreEntre} est de : " + conv.convertDecimalToHexa(conv.nombreEntre.toString().toInt()).toString()
                                }
            else -> {
                leResultat.text="Cette option n'est pas disponible pour le moment"
            }
        }

            btnRetour.setOnClickListener {
                val intentReturn = Intent(this, MainActivity::class.java)
                startActivity(intentReturn)
            }
        }
    }
}
