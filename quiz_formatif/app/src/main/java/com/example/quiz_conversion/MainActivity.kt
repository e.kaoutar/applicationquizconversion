package com.example.quiz_conversion

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //objet global : reutilisable dans la 2eme activite
    companion object {
        const val CONVERTER = "conv"
    }

    var pos : Int = 0
    var pos2 : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val options = arrayOf("Binaire", "Decimal")
        val spinner = findViewById<Spinner>(R.id.convertDe)
        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, options)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    //Toast.makeText(this@MainActivity, getString(R.string.) + " " + options[position], Toast.LENGTH_SHORT).show()
                pos2=position
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val optionsTo = arrayOf("Binaire", "Decimal", "Hexa", "Octal")
        val spinnerTo = findViewById<Spinner>(R.id.convertA)
        if (spinnerTo != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, optionsTo)
            spinnerTo.adapter = arrayAdapter

            spinnerTo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    //Toast.makeText(this@MainActivity, optionsTo[position], Toast.LENGTH_SHORT).show()
                    pos=position
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected-- Juste pour le test
                    //Toast.makeText(this@MainActivity, "Nothing selected", Toast.LENGTH_LONG ).show()
                }
            }
        }

        button2.setOnClickListener{
            //all my attributes are here
            var conv = laConversion(inputNumber.text.toString().toLong(), options[pos2], optionsTo[pos])
            //val alert = AlertDialog.Builder(this)
            //alert.setMessage("${conv.nombreEntre} va etre converti de ${conv.etatinitial} à ${conv.etatfinal}")
            //alert.show()

            val intent = Intent(this, ResultActivity::class.java)
            intent.putExtra(CONVERTER, conv)
            startActivity(intent)

        }
    }


}
