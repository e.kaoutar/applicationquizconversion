package com.example.quiz_conversion

import android.os.Parcel
import android.os.Parcelable


data class laConversion(var nombreEntre:Long, var etatinitial:String, var etatfinal:String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(nombreEntre)
        parcel.writeString(etatinitial)
        parcel.writeString(etatfinal)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<laConversion> {
        override fun createFromParcel(parcel: Parcel): laConversion {
            return laConversion(parcel)
        }

        override fun newArray(size: Int): Array<laConversion?> {
            return arrayOfNulls(size)
        }
    }

    fun convertBinaryToDecimal(num:Long) : Int
    {
        var nombreDepart = num
        var decimalNumber = 0
        var i = 0
        var remainder : Long

        while(nombreDepart.toInt() != 0){
            remainder = nombreDepart % 10
            nombreDepart /= 10
            decimalNumber += (remainder * Math.pow(2.0, i.toDouble())).toInt()
            i++
        }

        return decimalNumber
    }

    fun convertDecimalToBinary(num:Int):Long{
        var nombreDepart = num
        var binaryNumber : Long = 0
        var remainder : Int
        var i = 1
        var step = 1

        while(nombreDepart!=0)
        {
            remainder=nombreDepart%2
            nombreDepart /= 2
            binaryNumber += (remainder*i).toLong()
            i *= 10
        }

        return binaryNumber
    }

    fun convertBinaryToOctal(num:Long):Int{
        var nombreDepart = num
        var octalNumber = 0
        var decimalNumber = 0
        var i = 0

        //convertir en decimal en premier
        while(nombreDepart.toInt() != 0){
            decimalNumber += (nombreDepart % 10 * Math.pow(2.0, i.toDouble())).toInt()
            ++i
            nombreDepart /= 10
        }

        i=1

        //puis de decimal en octal
        while(decimalNumber != 0){
            octalNumber += decimalNumber % 8 * i
            decimalNumber /= 8
            i *= 10
        }
        return octalNumber
    }

    fun convertDecimalToOctal(decimal: Int): Int {
        var decimal = decimal
        var octalNumber = 0
        var i = 1
        while (decimal != 0) {
            octalNumber += decimal % 8 * i
            decimal /= 8
            i *= 10
        }
        return octalNumber
    }

    fun convertDecimalToHexa(decimal : Int):String{
        var decimal = decimal
        return "%x".format(decimal)
    }

    fun convertBinaryToHexa(binary : Long):String{
        var binary = binary
        return "%x".format(binary)
    }


}